DECLARE @CURSOR CURSOR,
@METHOD NVARCHAR(MAX),
@WORKORDERSID BIGINT,
@ID INT
BEGIN
    SELECT @ID=MAX(ID) FROM [dbo].[CAYENTA_API_CALLS]
    
    SET @CURSOR=CURSOR FOR
    SELECT METHOD,WORKORDERSID FROM [dbo].[CAYENTA_API_CALLS]
        WHERE ID<=@ID
    
    OPEN @CURSOR
    FETCH NEXT FROM @CURSOR INTO @METHOD,@WORKORDERSID
    WHILE @@FETCH_STATUS=0
    BEGIN
        EXEC [dbo].[CALL_WEB_WRAPPER] @METHOD,@WORKORDERSID
        WAITFOR DELAY '00:00:01'
        
        FETCH NEXT FROM @CURSOR INTO @METHOD,@WORKORDERSID
    END
    CLOSE @CURSOR
    DEALLOCATE @CURSOR
    
    DELETE FROM [dbo].[CAYENTA_API_CALLS]
        WHERE ID<=@ID
END