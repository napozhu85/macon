CREATE PROCEDURE [dbo].[CREATE_JOB_COST]
AS
DECLARE @CURSOR CURSOR,
@CURSOR2 CURSOR,
@PROJECTNAME NVARCHAR(130),
@PROJECTSID DECIMAL(10,0),
@DESCRIPTION NVARCHAR(130),
@PROJECTSTARTDATE DATETIME,
@PROJECTFINISHDATE DATETIME,
@APPROVEDBY NVARCHAR(30),
@ASSIGNEDTO NVARCHAR(30),
@CANCEL  NVARCHAR(1),
@STATUS NVARCHAR(50),
@COMMENTS NVARCHAR(250),
@QUICKVIEW NVARCHAR(1),
@DOMAINID DECIMAL(18,0),
@ESTIMATEDBUDGET DECIMAL(13,2),
@PARENTPROJECTSID DECIMAL(10,0),
@DATETIMEMODIFIED DATETIME,
@NOBUDGET BIT,
@INITIATEDBY NVARCHAR(100),
@INITIATEDATE DATETIME,
@COUNT INT
BEGIN
    SET NOCOUNT ON
    DELETE FROM [dbo].[TEMP_PROJECT]

    --JOB_AREA
    INSERT INTO [dbo].[TEMP_PROJECT]
        ([PROJECTNAME]
        ,[PROJECTSID]
        ,[DESCRIPTION]
        ,[STATUS]
        ,[COMMENTS]
        ,[QUICKVIEW]
        ,[DOMAINID]
        ,[PARENTPROJECTSID])
        SELECT [JOB_AREA] [PROJECTNAME]
            ,ROW_NUMBER() OVER (ORDER BY [JOB_AREA]) AS [PROJECTSID]
            ,[JOB_AREA] AS [DESCRIPTION]
            ,'A' AS [STATUS]
            ,'DO NOT PROCESS' AS [COMMENTS]
            ,'Y' AS [QUICKVIEW]
            ,'1' AS [DOMAINID]
            ,'0' AS [PARENTPROJECTSID]
            FROM (SELECT DISTINCT [JOB_AREA] FROM [CAY-PROD-SQL\CAYPRODSQL].[CayProd].[CAYENTA].[BI_CW_JOBCOST]
                WHERE [YR]=IIF(MONTH(SYSDATETIME())>=10,YEAR(SYSDATETIME())+1,YEAR(SYSDATETIME()))) JA

    --JOB_NO
    INSERT INTO [dbo].[TEMP_PROJECT]
        ([PROJECTNAME]
        ,[PROJECTSID]
        ,[DESCRIPTION]
        ,[STATUS]
        ,[COMMENTS]
        ,[QUICKVIEW]
        ,[DOMAINID]
        ,[PARENTPROJECTSID])
        SELECT 
            CASE WHEN L.[JOB_AREA]='EQ' AND (ISNUMERIC(L.[JOB_NO])=1) THEN RIGHT(L.[JOB_NO],4)
                WHEN L.[JOB_AREA]!='EQ' AND (ISNUMERIC(L.[JOB_NO])=1) THEN COALESCE(L.[JOB_NAME],'')
                ELSE L.[JOB_NO] END AS [PROJECTNAME]
            ,L.[ID] AS PROJECTSID
            ,L.[JOB_AREA]+'-'+L.[JOB_NO] AS [DESCRIPTION]
            ,L.[JOB_STATUS] AS [STATUS]
            ,L.[JOB_AREA]+'-'+L.[JOB_NO] AS [COMMENTS]
            ,'Y' AS [QUICKVIEW]
            ,'1' AS [DOMIANID]
            ,R.[PROJECTSID] [PARENTPROJECTSID]
            FROM (SELECT ROW_NUMBER() OVER (ORDER BY [JOB_AREA])+3 AS [ID]
                ,[JOB_AREA]
                ,RTRIM([JOB_NO]) AS [JOB_NO]
                ,LTRIM(RTRIM([TASK])) AS [TASK]
                ,LTRIM(RTRIM([SUB_TASK])) AS [SUB_TASK]
                ,[JOB_NAME]
                ,[JOB_STATUS]
                FROM [CAY-PROD-SQL\CAYPRODSQL].[CayProd].[CAYENTA].[BI_CW_JOBCOST]
                WHERE [YR] = IIF(MONTH(SYSDATETIME())>=10,YEAR(SYSDATETIME())+1,YEAR(SYSDATETIME()))) L  
            LEFT JOIN [dbo].[TEMP_PROJECT] R
                ON LTRIM(RTRIM(L.[JOB_AREA]))=R.[DESCRIPTION] COLLATE SQL_LATIN1_GENERAL_CP1_CS_AS
            WHERE L.[TASK]='' AND L.[SUB_TASK]=''
      
    --TASK
    INSERT INTO [dbo].[TEMP_PROJECT]
        ([PROJECTNAME]
        ,[PROJECTSID]
        ,[DESCRIPTION]
        ,[STATUS]
        ,[COMMENTS]
        ,[QUICKVIEW]
        ,[DOMAINID]
        ,[PARENTPROJECTSID])
        SELECT L.[JOB_NO]+'-'+L.[TASK] [PROJECTNAME]
            ,L.[ID] [PROJECTSID]
            ,L.[JOB_AREA]+'-'+L.[JOB_NO]+'-'+L.[TASK] AS [DESCRIPTION]
            ,L.[JOB_STATUS] AS [STATUS]
            ,L.[JOB_AREA]+'-'+L.[JOB_NO]+'-'+L.[TASK] AS [COMMENT]
            ,'Y' AS [QUICKVIEW]
            ,'1' AS [DOMAINID]
            ,R.[PROJECTSID] [PARENTPROJECTSID]
            FROM (SELECT ROW_NUMBER() OVER (ORDER BY [JOB_AREA])+3 AS [ID]
                ,[JOB_AREA]
                ,RTRIM([JOB_NO]) AS [JOB_NO]
                ,LTRIM(RTRIM([TASK])) AS [TASK]
                ,LTRIM(RTRIM([SUB_TASK])) AS [SUB_TASK]
                ,[JOB_STATUS]
                FROM [CAY-PROD-SQL\CAYPRODSQL].[CayProd].[CAYENTA].[BI_CW_JOBCOST]
                WHERE [YR]=IIF(MONTH(SYSDATETIME())>=10,YEAR(SYSDATETIME())+1,YEAR(SYSDATETIME()))) L  
            LEFT JOIN [dbo].[TEMP_PROJECT] R
                ON (L.[JOB_AREA]+'-'+L.[JOB_NO])=R.[DESCRIPTION] COLLATE SQL_LATIN1_GENERAL_CP1_CS_AS
            WHERE L.[TASK]!='' AND L.[SUB_TASK]=''

    --SUBTASK
    INSERT INTO [dbo].[TEMP_PROJECT]
        ([PROJECTNAME]
        ,[PROJECTSID]
        ,[DESCRIPTION]
        ,[STATUS]
        ,[COMMENTS]
        ,[QUICKVIEW]
        ,[DOMAINID]
        ,[PARENTPROJECTSID])
    SELECT L.[JOB_NO]+'-'+L.[TASK]+'-'+L.[SUB_TASK]+'-'+L.[JOB_NAME] AS [PROJECTNAME]
        ,L.[ID] [PROJECTSID]
        ,L.[JOB_AREA]+'-'+L.[JOB_NO]+'-'+L.[TASK]+'-'+L.[SUB_TASK] AS [DESCRIPTION]
        ,L.[JOB_STATUS] AS [STATUS]
        ,L.[JOB_AREA]+'-'+L.[JOB_NO]+'-'+L.[TASK]+'-'+L.[SUB_TASK] AS [COMMENT]
        ,'Y' AS [QUICKVIEW]
        ,'1' AS [DOMAINID]
        ,R.[PROJECTSID] [PARENTPROJECTSID]
        FROM (SELECT ROW_NUMBER() OVER (ORDER BY [JOB_AREA])+3 AS [ID]
            ,[JOB_AREA]
            ,RTRIM([JOB_NO]) AS [JOB_NO]
            ,LTRIM(RTRIM([TASK])) AS [TASK]
            ,LTRIM(RTRIM([SUB_TASK])) AS [SUB_TASK]
            ,[JOB_STATUS]
            ,[JOB_NAME]
            FROM [CAY-PROD-SQL\CAYPRODSQL].[CayProd].[CAYENTA].[BI_CW_JOBCOST]
            WHERE YR=IIF(MONTH(SYSDATETIME())>=10,YEAR(SYSDATETIME())+1,YEAR(SYSDATETIME()))) L  
        LEFT JOIN [dbo].[TEMP_PROJECT] R
            ON (L.[JOB_AREA]+'-'+L.[JOB_NO]+'-'+L.[TASK])=R.[DESCRIPTION] COLLATE SQL_LATIN1_GENERAL_CP1_CS_AS
        WHERE L.[TASK]!='' AND L.[SUB_TASK]!=''
      
    SELECT @COUNT=COUNT(*) FROM [dbo].[TEMP_PROJECT]
    IF @COUNT<3
    BEGIN
        RETURN
    END
      
    DELETE FROM [azteca].[PROJECT]
        WHERE [DESCRIPTION] NOT IN (SELECT [DESCRIPTION] FROM [dbo].[TEMP_PROJECT])
            
    SET @CURSOR = CURSOR FOR
    SELECT [PROJECTNAME]
        ,[PROJECTSID]
        ,[DESCRIPTION]
        ,[PROJECTSTARTDATE]
        ,[PROJECTFINISHDATE]
        ,[APPROVEDBY]
        ,[ASSIGNEDTO]
        ,[CANCEL]
        ,[STATUS]
        ,[COMMENTS]
        ,[QUICKVIEW]
        ,[DOMAINID]
        ,[ESTIMATEDBUDGET]
        ,[PARENTPROJECTSID]
        ,[DATETIMEMODIFIED]
        ,[NOBUDGET]
        ,[INITIATEDBY]
        ,[INITIATEDATE]
        ,[dbo].[GET_SUBSTRING_COUNT]([DESCRIPTION], '-') [COUNT]
        FROM [dbo].[TEMP_PROJECT]
        WHERE [DESCRIPTION] NOT IN (SELECT [DESCRIPTION] FROM [azteca].[PROJECT])
        ORDER BY [COUNT]
        
    OPEN @CURSOR
    FETCH NEXT FROM @CURSOR INTO @PROJECTNAME,
        @PROJECTSID,
        @DESCRIPTION,
        @PROJECTSTARTDATE,
        @PROJECTFINISHDATE,
        @APPROVEDBY,
        @ASSIGNEDTO,
        @CANCEL,
        @STATUS,
        @COMMENTS,
        @QUICKVIEW,
        @DOMAINID,
        @ESTIMATEDBUDGET,
        @PARENTPROJECTSID,
        @DATETIMEMODIFIED,
        @NOBUDGET,
        @INITIATEDBY,
        @INITIATEDATE,
        @COUNT

    WHILE @@FETCH_STATUS=0
    BEGIN
        IF @PARENTPROJECTSID > 0
        BEGIN
            SELECT @PARENTPROJECTSID=[PROJECTSID] FROM [azteca].[PROJECT]
                WHERE [DESCRIPTION] IN (SELECT [DESCRIPTION] FROM [dbo].[TEMP_PROJECT]
                    WHERE [PROJECTSID]=@PARENTPROJECTSID)
        END
        
        SELECT @PROJECTSID=MAX([PROJECTSID])+1 FROM [azteca].[PROJECT]
        IF @PROJECTSID IS NULL
        BEGIN
            SET @PROJECTSID=1
        END
        
        INSERT INTO [azteca].[PROJECT]
            ([PROJECTNAME]
            ,[PROJECTSID]
            ,[DESCRIPTION]
            ,[PROJECTSTARTDATE]
            ,[PROJECTFINISHDATE]
            ,[APPROVEDBY]
            ,[ASSIGNEDTO]
            ,[CANCEL]
            ,[STATUS]
            ,[COMMENTS]
            ,[QUICKVIEW]
            ,[DOMAINID]
            ,[ESTIMATEDBUDGET]
            ,[PARENTPROJECTSID]
            ,[DATETIMEMODIFIED]
            ,[NOBUDGET]
            ,[INITIATEDBY]
            ,[INITIATEDATE])
            VALUES
            (@PROJECTNAME,
            @PROJECTSID,
            @DESCRIPTION,
            @PROJECTSTARTDATE,
            @PROJECTFINISHDATE,
            @APPROVEDBY,
            @ASSIGNEDTO,
            @CANCEL,
            @STATUS,
            @COMMENTS,
            @QUICKVIEW,
            @DOMAINID,
            @ESTIMATEDBUDGET,
            @PARENTPROJECTSID,
            @DATETIMEMODIFIED,
            @NOBUDGET,
            @INITIATEDBY,
            @INITIATEDATE)

        FETCH NEXT FROM @CURSOR INTO @PROJECTNAME,
            @PROJECTSID,
            @DESCRIPTION,
            @PROJECTSTARTDATE,
            @PROJECTFINISHDATE,
            @APPROVEDBY,
            @ASSIGNEDTO,
            @CANCEL,
            @STATUS,
            @COMMENTS,
            @QUICKVIEW,
            @DOMAINID,
            @ESTIMATEDBUDGET,
            @PARENTPROJECTSID,
            @DATETIMEMODIFIED,
            @NOBUDGET,
            @INITIATEDBY,
            @INITIATEDATE,
            @COUNT
    END
    CLOSE @CURSOR
    DEALLOCATE @CURSOR
    
    SET @CURSOR2 = CURSOR FOR
    SELECT [PROJECTNAME]
        ,[DESCRIPTION]
        ,[COMMENTS]
        FROM [dbo].[TEMP_PROJECT]
        WHERE [DESCRIPTION] IN
            (SELECT [DESCRIPTION] FROM [azteca].[PROJECT])
        
    OPEN @CURSOR2
    FETCH NEXT FROM @CURSOR2 INTO @PROJECTNAME,
        @DESCRIPTION,
        @COMMENTS
        
    WHILE @@FETCH_STATUS = 0
    BEGIN
        UPDATE [azteca].[PROJECT]
            SET [PROJECTNAME]=@PROJECTNAME,
            [COMMENTS]=@COMMENTS
            WHERE [DESCRIPTION]=@DESCRIPTION
            
        FETCH NEXT FROM @CURSOR2 INTO @PROJECTNAME,
            @DESCRIPTION,
            @COMMENTS
    END
    CLOSE @CURSOR2
    DEALLOCATE @CURSOR2
END