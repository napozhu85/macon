﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace CityworksMaconWebWrapper
{
    [ServiceContract]
    public interface ICityworksService
    {
        [OperationContract()]
        [WebGet(UriTemplate = "AddWorkRequest/{woSid}", ResponseFormat = WebMessageFormat.Json)]
        bool AddWorkRequest(string woSid);

        [OperationContract()]
        [WebGet(UriTemplate = "AddWorkRequestResources/{woSid}", ResponseFormat = WebMessageFormat.Json)]
        bool AddWorkRequestResources(string woSid);

        [OperationContract()]
        [WebGet(UriTemplate = "CancelServiceOrder/{woSid}", ResponseFormat = WebMessageFormat.Json)]
        bool CancelServiceOrder(string woSid);

        [OperationContract()]
        [WebGet(UriTemplate = "CloseServiceOrder/{woSid}", ResponseFormat = WebMessageFormat.Json)]
        bool CloseServiceOrder(string woSid);

        [OperationContract()]
        [WebGet(UriTemplate = "SetServiceOrder/{woSid}", ResponseFormat = WebMessageFormat.Json)]
        bool SetServiceOrder(string woSid);

        [OperationContract()]
        [WebGet(UriTemplate = "SetServiceOrderComments/{woSid}", ResponseFormat = WebMessageFormat.Json)]
        bool SetServiceOrderComments(string woSid);

        [OperationContract()]
        [WebGet(UriTemplate = "ValidateSerialNo/{woSid}/{sn}/{isMeterSn}", ResponseFormat = WebMessageFormat.Json)]
        bool ValidateSerialNo(string woSid, string sn, string isMeterSn);
    }
}