using System;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CityworksMaconWebWrapper
{
    public class CityworksService : ICityworksService
    {
        private readonly string[] CodeMappings = new[] { "RESOLUTION CODE", "METER STATUS", "METER CHANGE TYPE", "METER STATUS - OLD", "METER STATUS - NEW", "DIAMETER" };
        private readonly Cityworks _context = new Cityworks();

        public bool AddWorkRequest(string woSid)
        {
            Task.Delay(TimeSpan.FromSeconds(1)).ContinueWith(_ =>
            {
                try
                {
                    var wo = GetWo(woSid);
                    if (wo is null) return;

                    var project = _context.PROJECTs.FirstOrDefault(p => p.PROJECTSID == wo.PROJECTSID);
                    if (project is null) return;

                    var woId = wo.WORKORDERID;
                    var jobArea = project.DESCRIPTION.Split(new[] { '-' })[0];
                    var jobNo = jobArea.Length + 2 > project.DESCRIPTION.Length ? "" : project.DESCRIPTION.Substring(jobArea.Length + 1);
                    var templateName = _context.WOTEMPLATEs.FirstOrDefault(w => w.WOTEMPLATEID == wo.WOTEMPLATEID)?.DESCRIPTION;
                    var data = $"<Request><AddWorkRequest><Params><WP_PREFIX>MWA</WP_PREFIX><WP_NUMBER>{woId}</WP_NUMBER><WO_PREFIX>MWA</WO_PREFIX><WO_NUMBER>{woId}</WO_NUMBER><WP_TYPE>CTW</WP_TYPE><WP_JOB_AREA>{jobArea}</WP_JOB_AREA><WP_JOB_NO>{jobNo}</WP_JOB_NO><WP_DESCRIPTION>{templateName?.Substring(0, Math.Min(templateName.Length, 40)) ?? ""}</WP_DESCRIPTION><WP_FULL_DESC>{templateName?.Substring(0, Math.Min(templateName.Length, 100)) ?? ""}</WP_FULL_DESC><CREATE_WO>Y</CREATE_WO></Params></AddWorkRequest></Request>";
                    PostCayentaData(woSid, data);
                }
                catch (Exception e)
                {
                    Log(e, "AddWorkRequest", woSid);
                }
            });
            return true;
        }

        public bool AddWorkRequestResources(string woSid)
        {
            Task.Delay(TimeSpan.FromSeconds(1)).ContinueWith(_ =>
            {
                try
                {
                    var wo = GetWo(woSid);
                    if (!HasValidProject(wo)) return;

                    var woId = wo.WORKORDERID;
                    var resources = _context.MATERIALCOSTPRJs.Where(m => m.WORKORDERSID == wo.WORKORDERSID).ToArray()
                        .Aggregate(new StringBuilder(),
                        (s, m) => s.Append($"<RESOURCE><RESOURCE_TYPE>IN</RESOURCE_TYPE><RESOURCE_ID>{m.MATERIALUID}</RESOURCE_ID><QUANTITY>{m.UNITSREQUIRED}</QUANTITY></RESOURCE>")).ToString();
                    var data = $"<Request><AddWorkRequestResources><Params><PREFIX>MWA</PREFIX><NUMBER>{woId}</NUMBER><ADD_MODE>F</ADD_MODE><LINE_NO></LINE_NO><RESOURCES>{resources}</RESOURCES></Params></AddWorkRequestResources></Request>";
                    PostCayentaData(woSid, data);
                }
                catch (Exception e)
                {
                    Log(e, "AddWorkRequestResources", woSid);
                }
            });
            return true;
        }

        public bool CancelServiceOrder(string woSid)
        {
            Task.Delay(TimeSpan.FromSeconds(1)).ContinueWith(_ =>
            {
                try
                {
                    var wo = GetWo(woSid);
                    if (wo is null) return;

                    if (_context.WOTEMPLATEs.FirstOrDefault(w => w.WOTEMPLATEID == wo.WOTEMPLATEID)?.APPLYTOENTITY == "WSERVICECONNECTION")
                    {
                        var woId = wo.WORKORDERID;
                        var data = $"<Request><CloseServiceOrder><Params><ACTION_NO></ACTION_NO><FOREIGN_ID>{woId}</FOREIGN_ID><ACTION_STAT></ACTION_STAT><ACTION_CMTS></ACTION_CMTS><RESOLUTION_CD>CNCLD</RESOLUTION_CD><RESOLUTION_DT>{(wo.DATECANCELLED.HasValue ? FormatDate(wo.DATECANCELLED) : FormatDate(wo.ACTUALFINISHDATE))}</RESOLUTION_DT><CLOSE_DTM></CLOSE_DTM><RESOLVED_BY>CITYWRKS</RESOLVED_BY><STOP_SCREENS_YN></STOP_SCREENS_YN><RESOURCE_TP></RESOURCE_TP><RESOURCE_ID></RESOURCE_ID></Params></CloseServiceOrder></Request>";
                        PostCayentaData(woSid, data);
                    }

                    if (HasValidProject(wo)) CloseWorkRequest(woSid);

                    wo.TEXT2 = "PROCESSED";
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    Log(e, "CancelServiceOrder", woSid);
                }
            });
            return true;
        }

        public bool CloseServiceOrder(string woSid)
        {
            Task.Delay(TimeSpan.FromSeconds(1)).ContinueWith(_ =>
            {
                try
                {
                    var resolution = GetWoCustFieldValue(woSid, "RESOLUTION CODE");
                    if (resolution == "CNCLD")
                    {
                        CancelServiceOrder(woSid);
                        return;
                    }

                    var wo = GetWo(woSid);
                    if (wo is null) return;

                    var template = _context.WOTEMPLATEs.FirstOrDefault(t => t.WOTEMPLATEID == wo.WOTEMPLATEID);
                    if (template is null) return;

                    if (template.APPLYTOENTITY == "WSERVICECONNECTION")
                    {
                        var woId = wo.WORKORDERID;
                        var mapping = _context.READ_TYPE_MAPPING.FirstOrDefault(m => m.ACTION == template.DESCRIPTION && m.RESOLUTION == resolution);
                        var data = $"<Request><CloseServiceOrder><Params><ACTION_NO></ACTION_NO><FOREIGN_ID>{woId}</FOREIGN_ID><ACTION_STAT></ACTION_STAT><ACTION_CMTS></ACTION_CMTS><RESOLUTION_CD>{resolution}</RESOLUTION_CD><RESOLUTION_DT>{FormatDate(wo.ACTUALFINISHDATE)}</RESOLUTION_DT><CLOSE_DTM>{FormatDateTime(wo.DATEWOCLOSED)}</CLOSE_DTM><RESOLVED_BY>CITYWRKS</RESOLVED_BY><STOP_SCREENS_YN></STOP_SCREENS_YN><RESOURCE_TP></RESOURCE_TP><RESOURCE_ID></RESOURCE_ID>{(GetWoCustFieldValue(woSid, "SERIAL NUMBER") == "" ? "<METERS/>" : $"<METERS><METER><SERIAL_NO>{GetWoCustFieldValue(woSid, "SERIAL NUMBER")}</SERIAL_NO><COMPANY_CD>NEP</COMPANY_CD><SERVICE_PURPOSE></SERVICE_PURPOSE>{(mapping?.MTR_CHG_FIRST != null ? $"<MTR_CHG_TP>{mapping.MTR_CHG_FIRST}</MTR_CHG_TP>" : "")}<LOCATION_NO>{GetWoCustFieldValue(woSid, "LOCATION NUMBER")}</LOCATION_NO><SERVICE_SEQ>{GetWoCustFieldValue(woSid, "SERVICE SEQUENCE")}</SERVICE_SEQ><CONNECTION_NO></CONNECTION_NO><MULTIPLIER></MULTIPLIER><READ_TP>{mapping?.FIRST ?? ""}</READ_TP><READ_DT>{FormatDate(wo.ACTUALFINISHDATE)}</READ_DT><READ_TM>{FormatTime(wo.ACTUALFINISHDATE)}</READ_TM><READ_ENTRY_CD></READ_ENTRY_CD><COPY_ATTRIBUTES_YN></COPY_ATTRIBUTES_YN><REGISTERS>{GetRegister(woSid, "HI/MAIN", " - CURRENT")}{GetRegister(woSid, "LO", " - CURRENT")}</REGISTERS></METER>{(GetWoCustFieldValue(woSid, "NEW METER SERIAL NUMBER") == "" ? "" : $"<METER><SERIAL_NO>{GetWoCustFieldValue(woSid, "NEW METER SERIAL NUMBER")}</SERIAL_NO><COMPANY_CD>NEP</COMPANY_CD><SERVICE_PURPOSE></SERVICE_PURPOSE>{(mapping?.MTR_CHG_SECOND != null ? $"<MTR_CHG_TP>{mapping.MTR_CHG_SECOND}</MTR_CHG_TP>" : "")}<LOCATION_NO></LOCATION_NO><SERVICE_SEQ></SERVICE_SEQ><CONNECTION_NO></CONNECTION_NO><MULTIPLIER></MULTIPLIER><READ_TP>{mapping?.SECOND ?? ""}</READ_TP><READ_DT>{FormatDate(wo.ACTUALFINISHDATE)}</READ_DT><READ_TM>{FormatTime(wo.ACTUALFINISHDATE)}</READ_TM><READ_ENTRY_CD></READ_ENTRY_CD><COPY_ATTRIBUTES_YN></COPY_ATTRIBUTES_YN><REGISTERS>{GetRegister(woSid, "HI/MAIN", " - NEW")}{GetRegister(woSid, "LO", " - NEW")}</REGISTERS></METER>")}</METERS>")}<DEVICES/></Params></CloseServiceOrder></Request>";
                        PostCayentaData(woSid, data);
                    }

                    if (HasValidProject(wo)) CloseWorkRequest(woSid);

                    wo.TEXT2 = "PROCESSED";
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    Log(e, "CloseServiceOrder", woSid);
                }
            });
            return true;
        }

        public bool SetServiceOrder(string woSid)
        {
            Task.Delay(TimeSpan.FromSeconds(1)).ContinueWith(_ =>
            {
                try
                {
                    var wo = GetWo(woSid);
                    if (wo is null || _context.WOTEMPLATEs.FirstOrDefault(w => w.WOTEMPLATEID == wo.WOTEMPLATEID)?.APPLYTOENTITY != "WSERVICECONNECTION") return;

                    var woId = wo.WORKORDERID;
                    var data = $"<Request><SetServiceOrder><Params><ACTION_NO></ACTION_NO><FOREIGN_ID>{woId}</FOREIGN_ID><SCHEDULED_DT>{FormatDate(wo.ACTUALFINISHDATE)}</SCHEDULED_DT><SCHEDULED_TM>{FormatTime(wo.ACTUALFINISHDATE)}</SCHEDULED_TM><FIRST_PRINTED_DT>{FormatDate(DateTime.Today)}</FIRST_PRINTED_DT><FIRST_PRINTED_TM>{FormatTime(DateTime.Today)}</FIRST_PRINTED_TM><ACTION_STAT>WOCR</ACTION_STAT></Params></SetServiceOrder></Request>";
                    PostCayentaData(woSid, data);
                }
                catch (Exception e)
                {
                    Log(e, "SetServiceOrder", woSid);
                }
            });
            return true;
        }

        public bool SetServiceOrderComments(string commentId)
        {
            Task.Delay(TimeSpan.FromSeconds(1)).ContinueWith(_ =>
            {
                try
                {
                    int.TryParse(commentId, out var id);
                    var comment = _context.WORKORDERCOMMENTs.FirstOrDefault(w => w.COMMENTID == id);
                    if (comment is null) return;

                    var author = _context.EMPLOYEEs.FirstOrDefault(e => e.EMPLOYEESID == comment.AUTHORSID);
                    if (author is null) return;

                    var woSid = comment.WORKORDERSID.ToString();
                    var wo = GetWo(woSid);
                    if (wo is null || _context.WOTEMPLATEs.FirstOrDefault(w => w.WOTEMPLATEID == wo.WOTEMPLATEID)?.APPLYTOENTITY != "WSERVICECONNECTION") return;

                    var data = $"<Request><AddServiceOrderComment><Params><ACTION_NO>{wo.TEXT1}</ACTION_NO><COMMENT_CD>CW</COMMENT_CD><CHANGE_CMTS>{comment.COMMENTS} (Created by: {author.UNIQUENAME})</CHANGE_CMTS></Params></AddServiceOrderComment></Request>";
                    PostCayentaData(woSid, data);
                }
                catch (Exception e)
                {
                    Log(e, "SetServiceOrderComments");
                }
            });
            return true;
        }

        public bool ValidateSerialNo(string woSid, string sn, string isMeterSn)
        {
            try
            {
                long.TryParse(woSid, out var id);
                bool.TryParse(isMeterSn, out var meterSn);
                sn = sn.Replace("ASTERISK", "*");
                var isValid = new ObjectParameter("VALID", typeof(bool));
                _context.VALIDATE_SERIAL_NO(id, sn, meterSn, isValid);
                return (bool)isValid.Value;
            }
            catch (Exception e)
            {
                Log(e, "ValidateSerialNo", woSid);
            }
            return false;
        }

        private void CloseWorkRequest(string woSid)
        {
            var wo = GetWo(woSid);
            var woId = wo.WORKORDERID;
            PostCayentaData(woSid, $"<Request><CloseWorkRequest><Params><PREFIX>MWA</PREFIX><NUMBER>{woId}</NUMBER><LINE_NO></LINE_NO></Params></CloseWorkRequest></Request>");
        }

        private string FormatDate(DateTime? date) => date.HasValue ? date.Value.ToString("yyyyMMdd") : "";

        private string FormatDateTime(DateTime? dateTime) => dateTime.HasValue ? dateTime.Value.ToString("yyyyMMddHHmmss") : "";

        private string FormatTime(DateTime? date) => date.HasValue ? date.Value.ToString("HHmmss") : "";

        private string GetRegister(string woSid, string prefix, string postfix = "")
        {
            var amount = GetWoCustFieldValue(woSid, $"{prefix} READING{postfix}", $"{prefix} READING");
            if (string.IsNullOrWhiteSpace(amount) || !decimal.TryParse(amount, out var readAmount) || readAmount < 0) return "";

            return $"<REGISTER><REGISTER_NO>{(prefix == "LO" ? "2" : "1")}</REGISTER_NO><READ_AMT>{amount}</READ_AMT><READER_EXCEPTION_CD></READER_EXCEPTION_CD><RESST_READ_AMT></RESST_READ_AMT></REGISTER>";
        }

        private WORKORDER GetWo(string woSid)
        {
            long.TryParse(woSid, out var id);
            return _context.WORKORDERs.FirstOrDefault(w => w.WORKORDERSID == id && w.TEXT1 != null);
        }

        private string GetWoCustFieldValue(string woSid, string name, string name2 = "")
        {
            var fieldName = name;
            var fieldValue = GetCustFieldValue(woSid, fieldName);
            if (fieldValue is null)
            {
                fieldName = name2;
                fieldValue = GetCustFieldValue(woSid, fieldName);
            }
            if (fieldValue is null) return "";

            return CodeMappings.Contains(fieldName) ? _context.PWCODEs.Where(p => p.DESCRIPTION == fieldValue)
                .Join(_context.PWCODETYPEs.Where(c => c.MODULE == "C"),
                p => p.CODETYPE, c => c.CODETYPE,
                (p, c) => p.CODE).FirstOrDefault() ?? "" : fieldValue;
        }

        private string GetCustFieldValue(string woSid, string name)
        {
            if (!long.TryParse(woSid, out var id)) Log(new Exception("WoSid is invalid."), "GetCustFieldValue", woSid);
            return _context.WOCUSTFIELDs.FirstOrDefault(w => w.WORKORDERSID == id && w.CUSTFIELDNAME == name)?.CUSTFIELDVALUE;
        }

        private bool HasValidProject(WORKORDER wo)
        {
            if (wo is null) return false;

            return _context.PROJECTs.Any(p => p.PROJECTSID == wo.PROJECTSID);
        }

        private void Log(Exception e, string method, string woSid = null)
        {
            using (var file = File.Open($"{AppDomain.CurrentDomain.BaseDirectory}\\Log.txt", FileMode.Append))
            {
                using (var writer = new StreamWriter(file))
                {
                    writer.WriteLine($"Time: {DateTime.Now}");
                    if (woSid != null) writer.WriteLine($"WO#: {woSid}");
                    writer.WriteLine(method);
                    writer.WriteLine($"Error: {e.Message}");
                    writer.WriteLine(e.StackTrace);
                    if (!(e.InnerException is null))
                    {
                        writer.WriteLine($"Error: {e.InnerException.Message}");
                        writer.WriteLine(e.InnerException.StackTrace);
                    }
                    writer.WriteLine();
                }
            }
        }

        private XmlDocument PostCayentaData(string woSid, string data)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12
                | SecurityProtocolType.Ssl3;
            var bytes = Encoding.ASCII.GetBytes(data);
            var request = WebRequest.Create(new Uri(ConfigurationManager.AppSettings["CayentaAPI"]));
            request.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["CayentaUserID"],
                ConfigurationManager.AppSettings["CayentaPassword"]);
            request.PreAuthenticate = true;
            request.ContentType = "text/xml";
            request.Method = "POST";
            request.ContentLength = bytes.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            var log = "";
            var reply = new XmlDocument();
            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        log = reader.ReadToEnd();
                        reply.LoadXml(log);
                    }
                }
            }

            using (var file = File.Open($"{AppDomain.CurrentDomain.BaseDirectory}\\Log.txt", FileMode.Append))
            {
                using (var writer = new StreamWriter(file))
                {
                    writer.WriteLine($"Time: {DateTime.Now}");
                    writer.WriteLine($"WO#: {woSid}");
                    writer.WriteLine("Request:");
                    writer.WriteLine(data);
                    writer.WriteLine("Response:");
                    writer.WriteLine(log.Trim());
                    writer.WriteLine();
                }
            }
            return reply;
        }
    }
}