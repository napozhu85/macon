﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Net;
using System.Text;
using System.Web;
using static System.Console;

namespace InspectionCreator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            WriteLine("");

            var token = Authenticate();
            byte[] data;
            using (var client = new WebClient())
            {
                data = client.DownloadData(ConfigurationManager.AppSettings["GISServiceUrl"]);
            }

            var json = Encoding.UTF8.GetString(data);
            var jObj = JObject.Parse(json);
            var features = (JArray)jObj["features"];
            foreach (var feature in features)
            {
                var geometry = feature["geometry"];
                var x = (decimal)geometry["x"];
                var y = (decimal)geometry["y"];
                WriteLine(CreateInspection(ConfigurationManager.AppSettings["EntityType"],
                    ConfigurationManager.AppSettings["InspTemplateId"], x, y, token));
            }
        }

        private static string Authenticate()
        {
            try
            {
                byte[] data;
                using (var client = new WebClient())
                {
                    data = client.DownloadData($"{ConfigurationManager.AppSettings["CityworksAPIUrl"]}Authentication/Authenticate?data={{'LoginName':'{ConfigurationManager.AppSettings["CityworksAPIUserName"]}','Password':'{ConfigurationManager.AppSettings["CityworksAPIPassword"]}'}}");
                }

                var json = Encoding.UTF8.GetString(data);
                var jObj = JObject.Parse(json);
                return HttpUtility.UrlEncode(jObj["Value"]["Token"].ToString());
            }
            catch
            {
            }
            return null;
        }

        private static int CallAPI(string url, string returnField = null)
        {
            byte[] data;
            using (var client = new WebClient())
            {
                data = client.DownloadData($"{ConfigurationManager.AppSettings["CityworksAPIUrl"]}{url}");
            }

            var json = Encoding.UTF8.GetString(data);
            var jObj = JObject.Parse(json);
            return (int)jObj["Status"] == 0 && !string.IsNullOrWhiteSpace(returnField)
                ? (int)jObj["Value"][returnField] : 0;
        }

        private static decimal CreateInspection(string entityType, string inspTemplateId, decimal x, decimal y, string token) =>
            CallAPI($"AMS/Inspection/Create?token={token}&data={{'EntityType':'{entityType}','InspTemplateId':'{inspTemplateId}','X':'{x}','Y':'{y}'}}", "InspectionId");
    }
}