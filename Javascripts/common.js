//Change Begins
var cityworksServiceBaseUrl = "/CityworksMaconWebWrapper/CityworksService.svc/";

$(function () {
    if ($(location).attr("pathname").toLowerCase().indexOf("/workmanagement/wogeneraledit.aspx") > -1) {
        prependMethodsToButtonClickHandler("#ctl00_Toolbar_cmdSaveWO", "if(validateSerialNo())return false;");

        $("#ctl00_Main_grdCustomData_lstTemplate").attr("disabled", true);
        $("#ctl00_Main_ctl387").attr("readonly", true);
        SetReadOnly("LAST ACCOUNT READ (1)");
        SetReadOnly("LAST ACCOUNT READ (2)");
        SetReadOnly("CUSTOMER NAME");
        SetReadOnly("CUSTOMER ADDRESS");
        SetReadOnly("SERIAL NUMBER");
        SetReadOnly("LOCATION NUMBER");
        SetReadOnly("SERVICE SEQUENCE");
        SetReadOnly("METER STATUS");
        SetReadOnly("METER STATUS - OLD");
        SetReadOnly("METER STATUS - NEW");
    } else if ($(location).attr("pathname").toLowerCase().indexOf("/workmanagement/womatcostedit.aspx") > -1) {
        $("#ctl00_Main_optListActPrj_1").attr("checked", true);
        $("#ctl00_Main_optListActPrj_0").attr("disabled", true);
    }
});

function prependMethodsToButtonClickHandler(button, methods) {
    var $save = $(button);
    if ($save.length) {
        var js = $save.attr('onclick');
        js = methods + js;
        $save.attr('onclick', js);
    }
}

function SetReadOnly(fieldName) {
    var templateId = $("#ctl00_Main_grdCustomData_lstTemplate").val();
    if ($.inArray(templateId, ['31076', '31075', '31070', '31059', '31074', '31082', '31083', '31088']) > -1 && fieldName == "SERIAL NUMBER") {
        return;
    }
    $("[id^='ctl00_Main_grdCustomData_txtAnswer_'][title='" + fieldName + "']").attr("readonly", true);
    $("select[id^='ctl00_Main_grdCustomData_cboAnswer_']").parent().prev(":contains('" + fieldName + "')").next().find("select").attr("disabled", true);
}

function validateSerialNo() {
    if (sessionStorage.getItem('INVALIDSN') == 'true') {
        sessionStorage.setItem('INVALIDSN', 'false');
        return true;
    }

    var woId = $("#ctl00_Main_cboWorkOrderId").val();
    if (woId === undefined || woId == null || woId.trim() == "") {
        woId = $("#ctl00_Main_hidWorkOrderId").val();
        if (woId === undefined || woId == null || woId.trim() == "") {
            return false;
        }
    }

    var $sn = $("[id^='ctl00_Main_grdCustomData_txtAnswer_'][title='NEW METER SERIAL NUMBER']");
    if ($sn.length) {
        var sn = $sn.val().trim().replace(/\*/g, "ASTERISK");
        if (sn.length) {
            var flag = false;
            $.ajax({
                url: cityworksServiceBaseUrl + "ValidateSerialNo/" + woId + "/" + encodeURIComponent(sn) + "/true",
                async: false
            }).done(function (result) {
                flag = !result;
            });
            if (flag) {
                sessionStorage.setItem('INVALIDSN', 'true');
                alert("Invalid serial number or installed at another location.");
                return true;
            }
        }
    }

    var $miu = $("td:contains('NEW HI/MAIN MIU CHANGE'),td:contains('NEW LO MIU CHANGE'),td:contains('HI/MAIN MIU - NEW'),td:contains('LO MIU - NEW')").next()
        .find("[id^='ctl00_Main_grdCustomData_numAnswer_']");
    if (!$miu.length) {
        return false;
    }

    var flag2 = false;
    $.each($miu, function (index, value) {
        var miu = $(value).val().trim().replace(/\*/g, "ASTERISK");
        if (!miu.length) {
            return true;
        }

        $.ajax({
            url: cityworksServiceBaseUrl + "ValidateSerialNo/" + woId + "/" + encodeURIComponent(miu) + "/false",
            async: false
        }).done(function (result) {
            flag2 = !result;
            if (flag2) {
                return false;
            }
        });
    });
    if (flag2) {
        sessionStorage.setItem('INVALIDSN', 'true');
        alert("Invalid MIU or installed at another location.");
        return true;
    }
}
//Changes End

String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"");};String.prototype.ltrim=function(){return this.replace(/^\s+/,"");};String.prototype.rtrim=function(){return this.replace(/\s+$/,"");};String.prototype.format=function(){var args=arguments;return this.replace(/\{\{|\}\}|\{(\d+)\}/g,function(m,n){if(m=="{{"){return"{";}
if(m=="}}"){return"}";}
return args[n];});};function addLoadEvent(oFunction,oWindow){if((!oWindow)||(typeof oWindow!="object"))
oWindow=window;var oOldOnload=oWindow.onload;if(typeof oWindow.onload!="function")
oWindow.onload=oFunction;else{oWindow.onload=function(){oOldOnload();oFunction();};}}
function clearLoadEvents(oWindow){if((!oWindow)||(typeof oWindow!="object"))
oWindow=window;oWindow.onload=null;}
var postLoadEvents=function(){};function addPostLoadEvent(oFunction){if(typeof oFunction=="function"){var oOldPostLoadEvents=postLoadEvents;postLoadEvents=function(){oOldPostLoadEvents();oFunction();};}}
function clearPostLoadEvents(){postLoadEvents=function(){};}
function addUnloadEvent(oFunction,oWindow){if((!oWindow)||(typeof oWindow!="object"))
oWindow=window;var oOldOnload=oWindow.onunload;if(typeof oWindow.onunload!="function")
oWindow.onunload=oFunction;else{oWindow.onunload=function(){oOldOnload();oFunction();};}}
function clearUnloadEvents(oWindow){if((!oWindow)||(typeof oWindow!="object"))
oWindow=window;oWindow.onunload=null;}
function isArray(obj){if(obj!=null){if(obj.length)
return(typeof(obj.length)=="undefined")?false:true;}
return false;}
function commifyArray(obj,delimiter){if(typeof(delimiter)=="undefined"||delimiter==null){delimiter=",";}
var s="";if(obj==null||obj.length<=0){return s;}
for(var i=0;i<obj.length;i++){s=s+((s=="")?"":delimiter)+obj[i].toString();}
return s;}
function getSingleInputText(obj,use_default,delimiter){if(obj!=null){if(obj.type){switch(obj.type){case"radio":return"";case"checkbox":return(use_default)?obj.defaultChecked:obj.checked;case"text":case"hidden":case"textarea":case"number":return(use_default)?obj.defaultValue:obj.value;case"password":return((use_default)?null:obj.value);case"select-one":if(obj.options==null){return null;}
if(use_default){var o=obj.options;for(var i=0;i<o.length;i++){if(o[i].defaultSelected){return o[i].text;}}
return o[0].text;}
if(obj.selectedIndex<0){return null;}
return(obj.options.length>0)?obj.options[obj.selectedIndex].text:null;case"select-multiple":if(obj.options==null){return null;}
var values=new Array();for(var i=0;i<obj.options.length;i++){if((use_default&&obj.options[i].defaultSelected)||(!use_default&&obj.options[i].selected)){values[values.length]=obj.options[i].text;}}
return(values.length==0)?null:commifyArray(values,delimiter);}}else if(obj.tagName){switch(obj.tagName.toString().toLowerCase()){case"label":case"span":case"div":return obj.innerHTML;case"cw-markup-editor":return obj.value;}}}
if(typeof console!=="undefined")
console.log("Field type "+obj.type+" cannot be used with the getInput functions. (superCombo.js:getSingleInputText:e1)");return null;}
function getInputText(obj,delimiter){var use_default=(arguments.length>2)?arguments[2]:false;if(isArray(obj)&&(typeof(obj.type)=="undefined")){var values=new Array();for(var i=0;i<obj.length;i++){var v=getSingleInputText(obj[i],use_default,delimiter);if(v!=null){values[values.length]=v;}}
return commifyArray(values,delimiter);}
return getSingleInputText(obj,use_default,delimiter);}
function getSingleInputValue(obj,use_default,delimiter){if(!obj)
return'';if(obj.type){switch(obj.type){case"radio":case"checkbox":return(((use_default)?obj.defaultChecked:obj.checked)?obj.value:null);case"text":case"hidden":case"textarea":case"number":return(use_default)?obj.defaultValue:obj.value;case"password":return((use_default)?null:obj.value);case"cw-date-time-picker":return((use_default)?null:obj.value);case"select-one":if(obj.options==null)
return null;if(use_default){var o=obj.options;for(var i=0;i<o.length;i++){if(o[i].defaultSelected)
return o[i].value;}
return o[0].value;}
if(obj.selectedIndex<0)
return null;return(obj.options.length>0)?obj.options[obj.selectedIndex].value:null;case'select-multiple':if(obj.options==null)
return null;var values=new Array();for(var i=0;i<obj.options.length;i++){if((use_default&&obj.options[i].defaultSelected)||(!use_default&&obj.options[i].selected)){values[values.length]=obj.options[i].value;}}
return(values.length==0)?null:commifyArray(values,delimiter);}}else if(obj.tagName){switch(obj.tagName.toString().toLowerCase()){case"label":case"span":case"div":return obj.innerHTML;case"cw-markup-editor":return obj.value;}}
if(typeof console!=="undefined")
console.log("ERROR: Field type "+obj.type+" is not supported for this function");return null;}
function getInputValue(obj,delimiter){var use_default=(arguments.length>2)?arguments[2]:false;if(isArray(obj)&&(typeof(obj.type)=="undefined")){var values=new Array();for(var i=0;i<obj.length;i++){var v=getSingleInputValue(obj[i],use_default,delimiter);if(v!=null){values[values.length]=v;}}
return commifyArray(values,delimiter);}
return getSingleInputValue(obj,use_default,delimiter);}
function setSingleInputValue(obj,value){if(obj.type){switch(obj.type){case'radio':case'checkbox':if(value==true||value=='checked'){obj.checked=true;return true;}else{obj.checked=false;return false;}
case'text':case'hidden':case'textarea':case'password':case"number":obj.value=value;return true;case'select-one':case'select-multiple':var o=obj.options;for(var i=0;i<o.length;i++){if(o[i].value==value){o[i].selected=true;}else if((o[i].innerHTML)&&(o[i].innerHTML==value)){o[i].selected=true;}else if((o[i].innerText)&&(o[i].innerText==value)){o[i].selected=true;}else{o[i].selected=false;}}
return true;}}else if(obj.tagName){switch(obj.tagName.toString().toLowerCase()){case"label":case"span":case"div":obj.innerHTML=value;return true;case"cw-markup-editor":obj.value=value;return true;}}
if(typeof console!=="undefined")
console.log("ERROR: Field type "+obj.type+" is not supported in the setSingleInputValue function.");return false;}
function setInputValue(obj,value){var use_default=(arguments.length>1)?arguments[1]:false;if(isArray(obj)&&(typeof(obj.type)=="undefined")){for(var i=0;i<obj.length;i++){setSingleInputValue(obj[i],value);}}else{setSingleInputValue(obj,value);}}
function getAncestorNode(currentNode,strID){if(currentNode==null)
return null;if((currentNode.id!=null)&&(currentNode.id.substr(currentNode.id.length-strID.length)==strID))
return currentNode;if(currentNode.parentNode==null)
return null;return getAncestorNode(currentNode.parentNode,strID);}
function getDescendantNode(currentNode,strID){if(!currentNode)
return null;var childArray;if(currentNode.id!=null){childArray=currentNode.id.substr(currentNode.id.length-strID.length);if(childArray==strID)
return currentNode;}
childArray=currentNode.childNodes;for(var i=0;i<childArray.length;i++){var oNode=getDescendantNode(childArray[i],strID);if(oNode!=null)
return oNode;}
return null;}
function validNum(evt){evt=(evt)?evt:((window.event)?window.event:null);if(evt){var keyCode=(evt.keyCode)?evt.keyCode:evt.which;if(((keyCode>47)&&(keyCode<58))||(keyCode==8)){if(typeof evt!="undefined")
return evt.returnValue=true;else
return false;}}
if(typeof evt!="undefined")
return evt.returnValue=false;else
return false;}
function validateRequiredFields(){return cw.Pages.validateRequiredFields();}
var bolGridExpanded=true;function toggleExpandState(gridId){var oGrid=$find(gridId);if(oGrid){if(oGrid.bolGridSummarized)
oGrid.bolGridSummarized=false;oGrid.bolGridExpanded=oGrid.bolGridExpanded==undefined?false:!oGrid.bolGridExpanded;var rows=oGrid.MasterTableView.get_element().rows;for(var i=0;i<rows.length;i++){var button=oGrid.MasterTableView._getGroupExpandButton(rows[i]);if(button&&button.id.split("__")[2]=="0"){if(button.className.indexOf("rgExpand")>-1&&oGrid.bolGridExpanded||!oGrid.bolGridExpanded&&button.className.indexOf("rgCollapse")>-1)
oGrid.MasterTableView._toggleGroupsExpand(button,{"groupLevel":button.id.split("__")[2]});}}}else if(igtbl_getGridById){var oGrid=igtbl_getGridById(gridId);if(oGrid){bolGridExpanded=!bolGridExpanded;for(var i=0,length=oGrid.Rows.length;i<length;i++)
oGrid.Rows.getRow(i).setExpanded(bolGridExpanded);}}}
function getDateTimeString(date){var zerof=function(value){if(value<10)
return"0"+value;else
return value.toString();}
return zerof(date.getMonth()+1)+"/"+zerof(date.getDate())+"/"+zerof(date.getFullYear())+" "+zerof(date.getHours())+":"+zerof(date.getMinutes());}
if((window.name!="CwMap")&&(document.title)&&(document.title!="")){top.document.title=document.title;}
if(typeof(cw)=='undefined'){var cw={};}
cw.Pages=function(){var fieldLabelMap=[];var successBannerId;var warningBannerId;var errorBannerId;return{registerRequiredField:function(fieldId,linkedControlId){fieldLabelMap[fieldId]=linkedControlId;},getRequiredFields:function(){return fieldLabelMap;},validateRequiredFields:function(){var ret=true;var fieldLabelMap=cw.Pages.getRequiredFields();for(fieldId in fieldLabelMap){var field=document.getElementById(fieldId);if(field){var valid=cw.Pages.validateField(field);var label=$("#"+fieldLabelMap[fieldId]);if(!label)
label=field.parentNode;if(valid){$(label).removeClass('required');}else{$(label).addClass('required');ret=false;}}}
return ret;},validateForm:function(bannerId){var valid=cw.Pages.validateRequiredFields();if(!valid){var msg=cw.Utilities.getFirstLayoutManager().Messages.RequiredFieldsError;cw.Pages.showBanner(msg,bannerId,true);return false;};return true;},validateTextBoxNumericRangeSelectorInputs:function(bannerId){if(!textBoxRangeSelector)
return true;var itemsWithErrors=textBoxRangeSelector.validateAllTextBoxRangeSelectorsOnPage();if(!itemsWithErrors||itemsWithErrors.length==0)
return true;var errorMsg="The numeric ranges in the following fields are not properly formatted: ";for(var cnt=itemsWithErrors.length,i=0;i<cnt;i++){errorMsg+=itemsWithErrors[i];if(i!==(cnt-1))
errorMsg+=", ";}
cw.Pages.showErrorBanner(errorMsg);return false;},validateField:function(oNode){var valid=true,value,text;var tag=oNode.tagName.toLowerCase();if(tag==="select"){value=getInputValue(oNode);text=getInputText(oNode);if(value==="-9999"&&text==="")
valid=false;else
valid=!(value===""||value===null);}else if(tag==="input"||tag==="textarea"){value=getInputValue(oNode);valid=!(value===""||value===null);}else if(tag==="table"){var inputs=oNode.getElementsByTagName("input");if(inputs.length>0){valid=false;for(var i=0;i<inputs.length;i++){if(inputs[i].checked){valid=true;break;}}}}else if(tag==="div"&&oNode.className.indexOf("RadComboBox")>-1){value=oNode.value;valid=!(value===""||value===null);}else if(tag==="cw-date-time-picker"){value=oNode.value;valid=!(value===""||value===null);}
return valid;},showBanner:function(bannerText,bannerId,autohide){autohide=typeof(autohide)=='undefined'?true:autohide;if(bannerText!=''){if(bannerId){var lbl=document.getElementById(bannerId);if(lbl){lbl.innerText=bannerText;lbl.innerHTML=bannerText;cw.Utilities.showHideElement(bannerId,'');var TIMEOUT=5000;if(autohide)
setTimeout('cw.Pages.hideBanner("'+bannerId+'");',TIMEOUT);}}else
alert(bannerText);}},hideBanner:function(bannerId){if(bannerId){var lbl=document.getElementById(bannerId);if(lbl){lbl.innerText='';lbl.innerHTML='';cw.Utilities.showHideElement(bannerId,'none');}}},setSuccessBannerId:function(id){successBannerId=id;},showSuccessBanner:function(text){cw.Pages.showBanner(text,successBannerId);},setWarningBannerId:function(id){warningBannerId=id;},showWarningBanner:function(text){cw.Pages.showBanner(text,warningBannerId);},setErrorBannerId:function(id){errorBannerId=id;},showErrorBanner:function(text){cw.Pages.showBanner(text,errorBannerId);}};}();cw.Window=function(){var strElementIdToFocus;var bolHandleFormKeystrokes=false;var controlIdsToSkip='';return{setInitialFocus:function(strTargetId){strElementIdToFocus=strTargetId;addLoadEvent(cw.Window.focusField);},setSize:function(width,height){if(window.outerWidth){window.outerWidth=width;window.outerHeight=height;}else if(window.resizeTo)
window.resizeTo(width,height);},getHeight:function(){var intHeight=600;if(typeof(window.innerHeight)=='number')
intHeight=window.innerHeight;else if(document.documentElement&&(document.documentElement.clientWidth||document.documentElement.clientHeight))
intHeight=document.documentElement.clientHeight;else if(document.body&&(document.body.clientWidth||document.body.clientHeight))
intHeight=document.body.clientHeight;return intHeight;},getWidth:function(){var intWidth;if(typeof(window.innerWidth)=='number')
intWidth=window.innerWidth;else if(document.documentElement&&(document.documentElement.clientWidth||document.documentElement.clientHeight))
intWidth=document.documentElement.clientWidth;else if(document.body&&(document.body.clientWidth||document.body.clientHeight))
intWidth=document.body.clientWidth;return intWidth;},focusField:function(target){if(target==null)
target=document.getElementById(strElementIdToFocus);if(target==null)
return;var formElements=["input.text","input.checkbox","input.radio","select","textarea"];var selectedNode=null;if(typeof document.selection!="undefined"&&document.selection!=null&&typeof window.opera=="undefined"){var theSelection=document.selection;var textRange=document.selection.createRange();selectedNode=textRange.parentElement();}
else if(typeof window.getSelection!="undefined"){var theSelection=window.getSelection();if(typeof theSelection.baseNode!="undefined")
selectedNode=theSelection.baseNode;else if(typeof theSelection.getRangeAt!="undefined"&&theSelection.rangeCount>0)
selectedNode=theSelection.getRangeAt(0).startContainer;}
if(selectedNode!=null)
for(var i=0;i<formElements.length;i++)
if(selectedNode.nodeName.toLowerCase()==formElements[i].replace(/([^.]*)\..*/,"$1"))
return;var forms=document.forms;for(i=0;i<forms.length;i++){formElements=forms[i];for(var j=0;j<formElements.length;j++){if(formElements[j].getAttribute("type")=="checkbox"||formElements[j].getAttribute("type")=="radio"){if(formElements[j].checked!=formElements[j].defaultChecked)
return;}else{if(typeof formElements[j].defaultValue!="undefined"&&formElements[j].value!=formElements[j].defaultValue)
return;}}}
target.focus();return;},handleFormOnKeyDown:function(){document.onkeydown=cw.Window.handleWindowKeystrokes;bolHandleFormKeystrokes=true;},handleWindowKeystrokes:function(oEvent){var intKeyCode;var target;if(window.event){intKeyCode=window.event.keyCode;target=window.event.target||window.event.srcElement;}else if(oEvent){intKeyCode=oEvent.which;target=oEvent.target||oEvent.srcElement;}
if(target.tagName!="TEXTAREA"){if(bolHandleFormKeystrokes){bolHandleFormKeystrokes=false;if(intKeyCode==13){if(controlIdsToSkip.indexOf(target.id,0)<0){if(window.event)
window.event.keyCode=intKeyCode=9;else
return false;}}}}},addControlIdToSkip:function(controlId){if(controlIdsToSkip.indexOf(controlId,0)<0){controlIdsToSkip+=controlId+",";}},scrollTo:function(position,animate){if((typeof Ext!="undefined")&&(position))
Ext.get(document.documentElement||document.body).scrollTo('top',parseInt(position),animate);},maintainScroll:function(formElement,memoryElement,animate){formElement=document.forms[formElement];memoryElement=document.getElementById(memoryElement);if(memoryElement.value)
cw.Window.scrollTo(memoryElement.value,animate);var storeVerticalOffset=function(){memoryElement.value=cw.Window.getVerticalOffset();};if(typeof formElement.onsubmit!="function")
formElement.onsubmit=storeVerticalOffset;else{var oldOnSubmit=formElement.onsubmit;formElement.onsubmit=function(){storeVerticalOffset();return oldOnSubmit();};}},getVerticalOffset:function(){return typeof window.pageYOffset!='undefined'?window.pageYOffset:document.documentElement&&document.documentElement.scrollTop?document.documentElement.scrollTop:document.body.scrollTop?document.body.scrollTop:0;},refreshEventLayers:function(){cw.Utilities.getRouter(function(r){r.send('loadEventLayers','loadEventLayers');});},performButtonClick:function(id){var btn=document.getElementById(id);if(btn&&typeof btn.onclick=="function")
btn.onclick();else
$('#'+id).trigger('click');},fireMouseEvent:function(mouseEvent,element){if(document.createEvent){var oEvent=document.createEvent("MouseEvents");oEvent.initMouseEvent(mouseEvent,true,true,window,0,0,0,0,0,false,false,false,false,0,null);element.dispatchEvent(oEvent);}}};}();cw.Pages.Search=function(){var oGroupByGrid,strGroupByGridId,oGridContainer,oPagerDestination,strSearchResultsGridId,intPageCount,intTotalRecords,intHeightOffset=150;function getGroupByGrid(){if((igtbl_getGridById)&&(strGroupByGridId))
oGroupByGrid=igtbl_getGridById(strGroupByGridId);}
return{setGroupByGridId:function(GroupByGridId){strGroupByGridId=GroupByGridId;},GroupByEntry:function(intID,bolApplied,strText,strValue){this.id=intID;this.applied=bolApplied;this.text=strText;this.value=strValue;this.getProperties=function(){var strProperties="ID: "+this.id+"\n"+"Applied: "+this.applied+"\n"+"Text: "+this.text+"\n"+"Value: "+this.value;return strProperties;};},updateGroupByGrid:function(oSelectBox){if((oSelectBox)&&(oSelectBox.options)){var arrGroupByEntries=new Array();var i,strRowId;for(i=0;i<oSelectBox.options.length;i++){if(oSelectBox.options[i].selected)
arrGroupByEntries[i+""]=new this.GroupByEntry(i,false,oSelectBox.options[i].text,oSelectBox.options[i].value);}
if(oGroupByGrid==null)
getGroupByGrid();if(oGroupByGrid){var colGroupByRows=oGroupByGrid.Rows;for(i=0;i<colGroupByRows.length;i++){strRowId=colGroupByRows.getRow(i).getCell(0).getValue()+"";if(!arrGroupByEntries[strRowId]){colGroupByRows.remove(i);i--;}}
for(var strKey in arrGroupByEntries){if(strKey!="length"){var oGroupByEntry=arrGroupByEntries[strKey];var bolFoundEntry=false;for(i=0;i<colGroupByRows.length;i++){strRowId=colGroupByRows.getRow(i).getCell(0).getValue()+"";if(strRowId==(oGroupByEntry.id+"")){bolFoundEntry=true;i=colGroupByRows.length;}}
if(!bolFoundEntry){var oNewRow=colGroupByRows.addNew();oNewRow.getCell(0).setValue(oGroupByEntry.id);oNewRow.getCell(1).setValue(oGroupByEntry.applied);oNewRow.getCell(2).setValue(oGroupByEntry.text);oNewRow.getCell(3).setValue(oGroupByEntry.value);}}}}}},moveUp:function(){if(oGroupByGrid==null)
getGroupByGrid();cw.UltraGrid.moveSelectedRowUp(oGroupByGrid);},moveDown:function(){if(oGroupByGrid==null)
getGroupByGrid();cw.UltraGrid.moveSelectedRowDown(oGroupByGrid);},setGridContainerHeight:function(gridContainerID){if(oGridContainer==null)
oGridContainer=document.getElementById(gridContainerID);if(oGridContainer){var intNewGridHeight=cw.Window.getHeight()-intHeightOffset;oGridContainer.style.height=intNewGridHeight+"px";}},processNewPage:function(gridId){try{var oPagerSource=document.getElementById("g-PagerSource");if(oPagerSource){if(oPagerDestination==null)
oPagerDestination=document.getElementById("g-PagerDestination");if(oPagerDestination){if(oPagerDestination.hasChildNodes())
oPagerDestination.removeChild(oPagerDestination.firstChild);var oPagerCopy=oPagerSource.cloneNode(true);oPagerCopy.id="g-PagerCopy";oPagerCopy.innerHTML=oPagerCopy.innerHTML.replace(/\{0\}/g,intTotalRecords);oPagerDestination.appendChild(oPagerCopy);var oGoToPageElement=oPagerCopy.getElementsByTagName("input")[0];if(oGoToPageElement){oGoToPageElement.setAttribute("onkeydown","return cw.Utilities.allowOnlyNumericKeys(event);");if(oGoToPageElement)
Ext.get(oGoToPageElement).addKeyListener([10,13],function(){cw.Pages.Search.goToPage(oGoToPageElement);});}}}}catch(e){}
if(igtbl_getGridById){var oGrid=igtbl_getGridById(gridId);if(oGrid){var arrRows=oGrid.Rows;if(arrRows.length>0&&typeof(this.expandAndStyleRows)!='undefined')
this.expandAndStyleRows(arrRows);}}
cw.UltraGrid.autoSizeGrid(gridId);},initializeResultsGrid:function(gridId){$(function(){var oPagerSource=document.getElementById("g-PagerSource");if(oPagerSource){oPagerSource.parentNode.parentNode.style.display="none";cw.Pages.Search.processNewPage(gridId);}else{var oDestination=document.getElementById("g-PagerDestination");intHeightOffset=130;oDestination.className="noPager";oDestination.innerHTML="Displaying "+intTotalRecords+" records.";}
if((intPageCount==null)||(intPageCount<=1))
cw.Pages.Search.processNewPage(gridId);if(cw.Print.printFriendly==false&&intTotalRecords!=1&&typeof(resultsGridContainer)!='undefined'){cw.Pages.Search.setGridContainerHeight(resultsGridContainer);window.onresize=function(){cw.Pages.Search.setGridContainerHeight(resultsGridContainer);};}
strSearchResultsGridId=gridId;});},expandAndStyleRows:function(rows){try{if(rows.getRow(0).ChildRowsCount>0){for(var i=0,length=rows.length;i<length;i++){var oRow=rows.getRow(i);oRow.setExpanded(true);this.expandAndStyleRows(oRow.getChildRows());}}else{for(var i=1,length=rows.length;i<length;i+=2)
rows.getRow(i).Element.className="g-Alternate";}}catch(e){}},goToPage:function(element){if(element.tagName.toLowerCase()=="button")
element=element.parentNode.getElementsByTagName("input")[0];var intPage=getInputValue(element);if(intPage){if(parseInt(intPage)>intPageCount)
intPage=intPageCount;igtbl_getGridById(strSearchResultsGridId).goToPage(intPage);}},setPageCount:function(pageCount){intPageCount=pageCount;},getPageCount:function(){return intPageCount;},setTotalRecordCount:function(totalRecords){intTotalRecords=totalRecords;},getTotalRecordCount:function(){return intTotalRecords;}};}();cw.Print=function(){var strPrintPreviewText="This is the print preview page and has limited functionality.";var strPrintButtonText="Print";var strBackButtonText="Close Preview";return{printFriendly:false,printPage:function(strPrintContainer,boolUseHistory,funcPrintSetup){cw.Print.printFriendly=true;if(strPrintContainer){var oContainer=document.getElementById(strPrintContainer);var fnPrintSetup=funcPrintSetup;if(oContainer){var oPrintPreview=document.createElement("span");oPrintPreview.className="print";oPrintPreview.innerHTML=strPrintPreviewText;oPrintPreview.appendChild(document.createElement("br"));var oPrintButton=document.createElement("button");oPrintButton.className="formButton";oPrintButton.onclick=function(){if(fnPrintSetup)
fnPrintSetup();window.print();};oPrintButton.setAttribute("type","button");oPrintButton.innerHTML=strPrintButtonText;oPrintPreview.appendChild(oPrintButton);var oBackButton=document.createElement("button");oBackButton.className="formButton";if(boolUseHistory&&boolUseHistory==true)
oBackButton.onclick=function(){history.go(-1);};else
oBackButton.onclick=function(){window.close();};oBackButton.setAttribute("type","button");oBackButton.innerHTML=strBackButtonText;oPrintPreview.appendChild(oBackButton);oContainer.appendChild(oPrintPreview);}}}};}();cw.UltraTree=function(){function findSelectedNode(node){if(node==null)
return null;if(node.getSelected())
return node;var oSelectedNode=findSelectedNode(node.getNextSibling());if(oSelectedNode!=null)
return oSelectedNode;if(node.hasChildren())
return findSelectedNode(node.getFirstChild());return null;}
return{getSelectedNode:function(tree){var oNode;var arrNodes=tree.getNodes();for(var i=0;i<arrNodes.length;i++){if(arrNodes[i].getSelected()){oNode=arrNodes[i];break;}else if(arrNodes[i].hasChildren()){oNode=findSelectedNode(arrNodes[i].getFirstChild());if(oNode)
break;}}
return oNode;},getNodeByTag:function(parentNode,tag){if(parentNode){if(parentNode.getTag()==tag)
return parentNode;var oTaggedNode=cw.UltraTree.getNodeByTag(parentNode.getNextSibling(),tag);if(oTaggedNode)
return oTaggedNode;oTaggedNode=cw.UltraTree.getNodeByTag(parentNode.getFirstChild(),tag);if(oTaggedNode)
return oTaggedNode;}
return null;}};}();cw.UltraGrid=function(){return{getFirstSelectedRow:function(oGroupByGrid){if((oGroupByGrid)&&(igtbl_getLength(oGroupByGrid.Rows)>0)){var intRowCount=igtbl_getLength(oGroupByGrid.Rows);for(var i=0;i<intRowCount;i++){var oRow=oGroupByGrid.Rows.getRow(i);if((oRow)&&(oRow.getSelected()))
return oRow;}}
return null;},moveSelectedRowDown:function(oGroupByGrid){if(oGroupByGrid){var oRow=cw.UltraGrid.getFirstSelectedRow(oGroupByGrid);if(oRow){var intIndex=oRow.getIndex()+1;if(intIndex==oGroupByGrid.Rows.length-1){var oNewRow=oGroupByGrid.Rows.addNew();oRow.remove();oGroupByGrid.Rows.insert(oRow,intIndex);oNewRow.remove();}else if(intIndex<oGroupByGrid.Rows.length){oRow.remove();oGroupByGrid.Rows.insert(oRow,intIndex);}}}},moveSelectedRowUp:function(oGroupByGrid){if(oGroupByGrid){var oRow=cw.UltraGrid.getFirstSelectedRow(oGroupByGrid);if(oRow){var intIndex=oRow.getIndex()-1;if(intIndex>-1){oRow.remove();oGroupByGrid.Rows.insert(oRow,intIndex);}}}},getRowByKeyValue:function(grid,key,value){if((grid)&&(key)&&(value)){var oRows=grid.Rows;var oRow;for(var i=0;i<oRows.length;i++){oRow=oRows.getRow(i);if(oRow.getCellFromKey(key).getValue()==value)
return oRow;}}
return null;},dataBind:function(gridId,dataSource){if(dataSource!=null&&dataSource.length>0){var props=[]
var grid=igtbl_getGridById(gridId);for(var prop in dataSource[0]){if(grid.Bands[0].getColumnFromKey(prop)!=null)
props.push(prop);}
for(var i=0;i<dataSource.length;i++){row=grid.Rows.addNew();for(var j=0;j<props.length;j++)
row.getCellFromKey(props[j]).setValue(dataSource[i][props[j]]);}}},autoSizeGrid:function(gridId){if(typeof(Ext)!='undefined'){if(Ext.isIE)
return;}
if(typeof(igtbl_getGridById)=='undefined')
return;try{var oGrid=igtbl_getGridById(gridId);if(!oGrid)
return;if(!oGrid.Bands)
return;var oBands=oGrid.Bands;if(!oBands)
return;var oBand=oBands[0];if(!oBand)
return;var oColumns=oBand.Columns;if(!oColumns)
return;var count=oColumns.length;var colArray=null;if(colArray==null){colArray=new Array();for(var i=0;i<count;i++){colArray[i]=0;}}
var oColHeaders;for(var i=0;i<count;i++){if(!oColumns[i].ServerOnly&&!oColumns[i].Hidden){oColHeaders=oGrid.MainGrid.getElementsByTagName('th')
for(var j=0;j<oColHeaders.length;j++){if(oColHeaders[j].getAttribute('id')==oColumns[i].Id){var colWidth=oColHeaders[j].firstChild.offsetWidth
if(colArray[i]<colWidth){colArray[i]=colWidth;}
if(colWidth>0){break;}}}}}
this.ParseNonGroupingRows(oGrid.Rows,oColumns,colArray);for(var i=0;i<count;i++){if(colArray[i]>0){oColumns[i].setWidth(colArray[i]+10);}}}catch(e){}},ParseNonGroupingRows:function(rowList,colList,colWidthArray){if(rowList.getRow(0).GroupByRow){for(var i=0;i<rowList.length;i++){this.ParseNonGroupingRows(rowList.getRow(i).Rows,colList,colWidthArray)}}else{this.GetRowsCellWidth(rowList,colList,colWidthArray);}},GetRowsCellWidth:function(rowList,colList,colWidthArray){for(var i=0;i<rowList.length;i++){for(var n=0;n<colList.length;n++){if(!colList[n].ServerOnly&&!colList[n].Hidden){var cell=rowList.getRow(i).getCell(n);if(colWidthArray[n]<cell.Element.firstChild.offsetWidth){colWidthArray[n]=cell.Element.firstChild.offsetWidth;}}}}},autoSizeGridOLD:function(gridId){try{var grid=igtbl_getGridById(gridId);var band=grid.Bands[0];var columns=band.Columns;var rows=grid.Rows;for(var columnIndex=0;columnIndex<columns.length;columnIndex++){var col=columns[columnIndex];var length=0;for(var rowIndex=0;rowIndex<rows.length;rowIndex++){var row=rows.getRow(rowIndex);var cell=row.getCell(columnIndex);if(cell.Element.firstChild.offsetWidth>length){length=cell.Element.firstChild.offsetWidth;}}
col.setWidth(length);}}catch(e){}}};}();cw.Shortcut={'all_shortcuts':{},'add':function(shortcut_combination,callback,opt){var default_options={'type':'keydown','propagate':false,'disable_in_input':false,'target':document,'keycode':false}
if(!opt)
opt=default_options;else{for(var dfo in default_options){if(typeof opt[dfo]=='undefined')
opt[dfo]=default_options[dfo];}}
var ele=opt.target
if(typeof opt.target=='string')
ele=document.getElementById(opt.target);var ths=this;shortcut_combination=shortcut_combination.toLowerCase();var func=function(e){e=e||window.event;if(opt['disable_in_input']){var element;if(e.target)
element=e.target;else if(e.srcElement)
element=e.srcElement;if(element.nodeType==3)
element=element.parentNode;if(element.tagName=='INPUT'||element.tagName=='TEXTAREA')
return;}
if(e.keyCode)
code=e.keyCode;else if(e.which)
code=e.which;var character=String.fromCharCode(code).toLowerCase();if(code==188)
character=",";if(code==190)
character=".";var keys=shortcut_combination.split("+");var kp=0;var shift_nums={"`":"~","1":"!","2":"@","3":"#","4":"$","5":"%","6":"^","7":"&","8":"*","9":"(","0":")","-":"_","=":"+",";":":","'":"\"",",":"<",".":">","/":"?","\\":"|"};var special_keys={'esc':27,'escape':27,'tab':9,'space':32,'return':13,'enter':13,'backspace':8,'scrolllock':145,'scroll_lock':145,'scroll':145,'capslock':20,'caps_lock':20,'caps':20,'numlock':144,'num_lock':144,'num':144,'pause':19,'break':19,'insert':45,'home':36,'delete':46,'end':35,'pageup':33,'page_up':33,'pu':33,'pagedown':34,'page_down':34,'pd':34,'left':37,'up':38,'right':39,'down':40,'f1':112,'f2':113,'f3':114,'f4':115,'f5':116,'f6':117,'f7':118,'f8':119,'f9':120,'f10':121,'f11':122,'f12':123};var modifiers={shift:{wanted:false,pressed:false},ctrl:{wanted:false,pressed:false},alt:{wanted:false,pressed:false},meta:{wanted:false,pressed:false}};if(e.ctrlKey)
modifiers.ctrl.pressed=true;if(e.shiftKey)
modifiers.shift.pressed=true;if(e.altKey)
modifiers.alt.pressed=true;if(e.metaKey)
modifiers.meta.pressed=true;for(var i=0;k=keys[i],i<keys.length;i++){if(k=='ctrl'||k=='control'){kp++;modifiers.ctrl.wanted=true;}else if(k=='shift'){kp++;modifiers.shift.wanted=true;}else if(k=='alt'){kp++;modifiers.alt.wanted=true;}else if(k=='meta'){kp++;modifiers.meta.wanted=true;}else if(k.length>1){if(special_keys[k]==code)
kp++;}else if(opt['keycode']){if(opt['keycode']==code)
kp++;}else{if(character==k)
kp++;else{if(shift_nums[character]&&e.shiftKey){character=shift_nums[character];if(character==k)
kp++;}}}}
if(kp==keys.length&&modifiers.ctrl.pressed==modifiers.ctrl.wanted&&modifiers.shift.pressed==modifiers.shift.wanted&&modifiers.alt.pressed==modifiers.alt.wanted&&modifiers.meta.pressed==modifiers.meta.wanted){callback(e);if(!opt['propagate']){e.cancelBubble=true;e.returnValue=false;if(e.stopPropagation){e.stopPropagation();e.preventDefault();}
return false;}}};this.all_shortcuts[shortcut_combination]={'callback':func,'target':ele,'event':opt['type']};if(ele.addEventListener)
ele.addEventListener(opt['type'],func,false);else if(ele.attachEvent)
ele.attachEvent('on'+opt['type'],func);else
ele['on'+opt['type']]=func;},'remove':function(shortcut_combination){shortcut_combination=shortcut_combination.toLowerCase();var binding=this.all_shortcuts[shortcut_combination];delete(this.all_shortcuts[shortcut_combination])
if(!binding)
return;var type=binding['event'];var ele=binding['target'];var callback=binding['callback'];if(ele.detachEvent)
ele.detachEvent('on'+type,callback);else if(ele.removeEventListener)
ele.removeEventListener(type,callback,false);else
ele['on'+type]=false;}};cw.Utilities=function(){var _verificationNeededForConfirm=true;var _confirmButtonId='';var _reopenId='';var _bypassYesNoMessage=false;function handleYesNoConfirmationResponse(results,buttonId,ctrlId){var oCtrl=document.getElementById(ctrlId);var resp=(results=="yes").toString();if(oCtrl)
setInputValue(oCtrl,resp);_bypassYesNoMessage=true;cw.Window.performButtonClick(buttonId);}
return{showHideElement:function(ids,displayValue){var tmpIds='';if(typeof(ids)=='object')
tmpIds=ids;if(typeof(ids)=='string')
tmpIds=new Array(ids);if(tmpIds!=''){for(var i in tmpIds){var oId=tmpIds[i];if(typeof(oId)!='undefined'){var oElement=document.getElementById(oId);if((oElement!=null)&&(oElement.style))
oElement.style.display=displayValue;}}}
return false;},validateFinishDate:function(dateStartInputId,dateFinishInputId){var oValidDate=true;var oStart=document.getElementById(dateStartInputId);var oFinish=document.getElementById(dateFinishInputId);if(oStart&&oFinish){var startTime=oStart.value;var finishTime=oFinish.value;if(startTime!==null&&finishTime!==null&&startTime>finishTime){oValidDate=false;}}
return oValidDate;},trimLength:function(obj){try{var mlength=obj.getAttribute?parseInt(obj.getAttribute("maxlength")):"";if(obj.getAttribute&&obj.value.replace(/\n/g,"\r\n").length>mlength){var extraLength=obj.value.replace(/\n/g,"\r\n").length-mlength;obj.value=obj.value.substring(0,obj.value.length-extraLength);}}catch(err){}},allowOnlyNumericKeys:function(e){var intKeyCode;if(window.event)
intKeyCode=e.keyCode;else if(e.which)
intKeyCode=e.which;if(intKeyCode)
return(intKeyCode==8||intKeyCode==9||intKeyCode==10||intKeyCode==13||intKeyCode==45||intKeyCode==46||(intKeyCode>=35&&intKeyCode<=40)||(intKeyCode>=48&&intKeyCode<=57)||(intKeyCode>=96&&intKeyCode<=107))},timeIt:function(func){var start=(new Date()).getTime();var ret=func();var end=(new Date()).getTime();if(typeof console!=="undefined")
console.log(end-start);else
alert(end-start);return ret;},parseJson:function(data){var ret=JSON.parse(data,function(key,value){if(typeof value==='string'){var isDate=/Date\(([-+]?\d+[-+]?\d+)\)/.exec(value);if(isDate){value=new Date(eval(isDate[1]));value.setSeconds(value.getSeconds()+1);}}
return value;});return ret;},doConfirm:function(buttonResults){var tmpId=_confirmButtonId;_confirmButtonId='';if(buttonResults=="yes"){_verificationNeededForConfirm=false;cw.Window.performButtonClick(tmpId);}},confirmCwItem:function(confirmButtonId,confirmTitle,confirmMessage){var res=true;if(_verificationNeededForConfirm==true){_confirmButtonId=confirmButtonId;if(typeof(Ext)!='undefined'){Ext.Msg.show({title:confirmTitle,msg:confirmMessage,buttons:Ext.Msg.YESNO,fn:this.doConfirm,icon:Ext.MessageBox.WARNING});}
res=false;}
_verificationNeededForConfirm=true;return res;},getGridRowCount:function(gridId){var grid=igtbl_getGridById(gridId);return grid.Rows.length;},getGridSelectedRowCount:function(gridId){var ret=0;var grid=igtbl_getGridById(gridId);for(var i=0;i<grid.Rows.length;i++){var row=grid.Rows.getRow(i);if(row.getSelected())
ret++;}
return ret;},selectGridRow:function(gridId,text){var grid=igtbl_getGridById(gridId);for(var i=0;i<grid.Rows.length;i++){var row=grid.Rows.getRow(i);if(row.find(text)!=null){row.setSelected();}}},getAjaxCallUrl:function(postfix,ajaxCall,params){var ret=window.location.href;if(postfix){var index=ret.search(/.aspx/i);ret=ret.substring(0,index)+postfix+ret.substring(index);}
if(ret.substring(ret.length-5,ret.length)=='.aspx')
ret+='?';else
ret+='&';ret+='ajaxcall='+ajaxCall;if(params){for(var i=0;i<params.length;i++)
ret+='&'+params[i].name+'='+encodeURIComponent(params[i].value);}
return ret;},getFirstLayoutManager:function(){var _layout=null;try{for(var i in cw.LayoutManagers){if(typeof(cw.LayoutManagers[i].Messages)!='undefined'){_layout=cw.LayoutManagers[i];break;}}}catch(e){}
return _layout;},boolReopening:false,doReOpenItem:function(buttonResults){var tmpId=_confirmButtonId;_confirmButtonId='';if(buttonResults=="yes"){var oReopen=document.getElementById(_reopenId);if(oReopen){setInputValue(oReopen,'y');_verificationNeededForConfirm=false;cw.Utilities.boolReopening=true;cw.Window.performButtonClick(tmpId);cw.Utilities.boolReopening=false;}}},confirmReOpenItem:function(confirmButtonId,confirmTitle,confirmMessage){var res=true;if(_verificationNeededForConfirm==true){_confirmButtonId=confirmButtonId;if(typeof(Ext)!='undefined'){Ext.Msg.show({title:confirmTitle,msg:confirmMessage,buttons:Ext.Msg.YESNO,fn:this.doReOpenItem,icon:Ext.MessageBox.WARNING});}
res=false;}
_verificationNeededForConfirm=true;return res;},reopenRecord:function(cwId,reopenId,saveId){_reopenId=reopenId;var oId=document.getElementById(cwId);var oIdValue='';if(oId)
oIdValue=getInputValue(oId,',');if(oIdValue.toString().indexOf(',')>0)
oIdValue=oIdValue.toString().substr(0,oIdValue.toString().indexOf(','));var oLayout=this.getFirstLayoutManager();var title=oLayout.Messages.ReOpenTitle+' '+oIdValue;var message=oLayout.Messages.ReOpenText+' '+oIdValue+'?';return cw.Utilities.confirmReOpenItem(saveId,title,message);},setFromStreetCode:function(pieces,cityId,stateId,zipId){var oTxt=document.getElementById(cityId);if(oTxt&&pieces[1])
setInputValue(oTxt,pieces[1]);oTxt=document.getElementById(stateId);if(oTxt&&pieces[2])
setInputValue(oTxt,pieces[2]);oTxt=document.getElementById(zipId);if(oTxt&&pieces[3])
setInputValue(oTxt,pieces[3]);},parseStreetCodes:function(itm,args,delimiter,cityId,stateId,zipId){if(itm){var textPieces=itm._element.value.toString().split(delimiter);var newText=textPieces[0];itm._element.value=newText;this.setFromStreetCode(textPieces,cityId,stateId,zipId);}},yesNoConfirm:function(confirmButtonId,responseControlId,title,text){if(_bypassYesNoMessage){_bypassYesNoMessage=false;return true;}
Ext.Msg.show({title:title,msg:text,buttons:Ext.Msg.YESNO,fn:function(buttonResults){handleYesNoConfirmationResponse(buttonResults,confirmButtonId,responseControlId);},icon:Ext.MessageBox.WARNING});return false;},inlineConfirm:function(confirmButtonId,text,yesText,noText,callback){var btn=$get(confirmButtonId);if(!btn)
return false;if(!btn.confirm){btn.showConfirmation=true;var s=document.createElement('span');var y=document.createElement('input');var n=document.createElement('input');y.className='formButton';y.value=yesText;y.id=btn.id+'Yes';n.type=y.type='button';n.className='formButton';n.value=noText;n.id=btn.id+'Cancel';var bgColor=$(btn.parentNode).css('background-color');if(bgColor==='transparent')
bgColor="#ffffff";btn.parentNode.insertBefore(s,btn.parentNode.firstChild);btn.confirm=$(s);btn.confirm.attr('style','min-height:20px;position:absolute;z-index:9500;');btn.confirm.css('background-color',bgColor||"#FFFFFF");btn.confirm.append("<label style='margin:5px;' class='label'>"+text+"</label>").append(y).append(n);$(y).on('click',function(){btn.showConfirmation=false;callback('yes');btn.showConfirmation=true;});$(n).on('click',function(){btn.confirm.hide();callback('cancel');});}else if(!btn.showConfirmation)
return true;else
btn.confirm.show();return false;},getRouter:function(callBack){if(cw.router){callBack(cw.router);}else{require(['js/factory'],function(factory){if(factory){var r=factory.createRouter("cw-router");if(cw&&cw.router)
callBack(cw.router);else
callBack(r);}});}},continueAnchorTemplate:function(config){if(config.location){var yesAnchor="<a href='"+config.location+"' style='color:#fff;'>"+config.yesText+"</a>";var noAnchor="<span onClick=\"cw.Utilities.showHideElement('"+config.elementId+"','none');\" style='color:#fff;cursor:pointer;text-decoration:underline;'>"+config.noText+"</span>";var msg=config.message+config.yesNoTemplate.format("<p/>",yesAnchor,noAnchor);return msg;}else
return config.message;}};}();cw.EventHub=function(){var messages=[];return{subscribe:function(message,key,func){if(!messages[message])
messages[message]=[];messages[message][key]=func;},unsubscribe:function(message,key){delete messages[message][key];},send:function(message,data){if(message){if(messages[message]){for(var key in messages[message])
messages[message][key](data);}else if(typeof console!=="undefined")
console.log('EVENTHUB: Message type ('+message+') does not exist.');}else if(typeof console!=="undefined")
console.log('EVENTHUB: Undefined message type.');}};}();cw.Dictionary=function(test){function isKey(k){var ret=true;if(k=='set'||k=='get'||k=='exists'||k=='remove'||k=='keys'||k=='values'||k=='kvps')
ret=false;return ret;}
var iStore={};return{set:function(key,value){if(isKey(key))
this[key]=function(){return value;}();else
iStore[key]=function(){return value;}();},get:function(key){if(isKey(key))
return this[key];return iStore[key];},exists:function(key){if(isKey(key))
return this[key]?true:false;return iStore[key]?true:false;},remove:function(key){if(isKey(key))
delete this[key];else
delete iStore[key];},keys:function(){var ret=[];for(var i in this){if(isKey(i))
ret[ret.length]=i;}
for(var j in iStore)
ret[ret.length]=j;return ret;},values:function(){var ret=[];for(var i in this){if(isKey(i))
ret[ret.length]=this[i];}
for(var j in iStore){ret[ret.length]=iStore[j];}
return ret;},kvps:function(){var ret={};for(var i in this){if(isKey(i))
ret[i]=this[i];}
for(var j in iStore)
ret[j]=iStore[j];return ret;}};};cw.LayoutManagers=new cw.Dictionary();cw.onReady=addLoadEvent;cw.animate=function(){var _isAnimating=false;return{set_isAnimating:function(val){_isAnimating=val;},isAnimating:function(){return _isAnimating;},opacity:function(id,opacStart,opacEnd,millisec){var speed=Math.round(millisec/100);var timer=0;if(opacStart>opacEnd){for(i=opacStart;i>=opacEnd;i--){setTimeout("cw.animate.changeOpac("+i+",'"+id+"')",(timer*speed));timer++;}}else if(opacStart<opacEnd){for(i=opacStart;i<=opacEnd;i++){setTimeout("cw.animate.changeOpac("+i+",'"+id+"')",(timer*speed));timer++;}}},changeOpac:function(opacity,id){try{var object=document.getElementById(id).style;object.opacity=(opacity/100);object.MozOpacity=(opacity/100);object.KhtmlOpacity=(opacity/100);object.filter="alpha(opacity="+opacity+")";_isAnimating=(opacity>0||opacity<100);}catch(e){}},shiftOpacity:function(id,millisec){if(document.getElementById(id).style.opacity==0){this.opacity(id,0,100,millisec);}else{this.opacity(id,100,0,millisec);}},blendimage:function(divid,imageid,imagefile,millisec){var speed=Math.round(millisec/100);var timer=0;document.getElementById(divid).style.backgroundImage="url("+document.getElementById(imageid).src+")";this.changeOpac(0,imageid);document.getElementById(imageid).src=imagefile;for(i=0;i<=100;i++){setTimeout("cw.animate.changeOpac("+i+",'"+imageid+"')",(timer*speed));timer++;}},currentOpac:function(id,opacEnd,millisec){var currentOpac=100;if(document.getElementById(id).style.opacity<100){currentOpac=document.getElementById(id).style.opacity*100;}
this.opacity(id,currentOpac,opacEnd,millisec)}};}();cw.includeScriptFile=function(pagePath,scriptPath){if(window.location.href.toLowerCase().indexOf(pagePath.toLowerCase())>-1){var scriptElement=document.createElement("script");scriptElement.src=scriptPath;document.documentElement.appendChild(scriptElement);}};if(typeof Range!=='undefined'&&typeof Range.prototype.createContextualFragment==='undefined'){Range.prototype.createContextualFragment=function(a){var b=window.document;var c=b.createElement('div');c.innerHTML=a;var d=b.createDocumentFragment(),e;if((e=c.firstChild)){d.appendChild(e);}
return d;};}